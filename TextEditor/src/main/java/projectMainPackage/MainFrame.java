package projectMainPackage;

import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MainFrame extends javax.swing.JFrame {

    public static Map<File, TextAreaTabPanel> map = new HashMap<>();

    public MainFrame() {
        initComponents();

        tpOpenedFiles.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
                if(panel != null) {
                    if (panel.getFile() != null) {
                        lStatusBar.setText(panel.getFilePath());
                    } else {
                        lStatusBar.setText("");
                    }
                } else {
                    lStatusBar.setText("");
                }
            }
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lStatusBar = new javax.swing.JLabel();
        tpOpenedFiles = new javax.swing.JTabbedPane();
        mbMainFrame = new javax.swing.JMenuBar();
        mFile = new javax.swing.JMenu();
        miNew = new javax.swing.JMenuItem();
        miOpen = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        miSaveAs = new javax.swing.JMenuItem();
        miClose = new javax.swing.JMenuItem();
        mHelp = new javax.swing.JMenu();
        miAbout = new javax.swing.JMenuItem();
        mWordWrap = new javax.swing.JMenu();
        miOn = new javax.swing.JMenuItem();
        miOff = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Text Editor");
        setPreferredSize(new java.awt.Dimension(1024, 768));

        mFile.setText("File");

        miNew.setText("New");
        miNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miNewActionPerformed(evt);
            }
        });
        mFile.add(miNew);

        miOpen.setText("Open");
        miOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miOpenActionPerformed(evt);
            }
        });
        mFile.add(miOpen);

        miSave.setText("Save");
        miSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveActionPerformed(evt);
            }
        });
        mFile.add(miSave);

        miSaveAs.setText("Save as");
        miSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveAsActionPerformed(evt);
            }
        });
        mFile.add(miSaveAs);

        miClose.setText("Close");
        miClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCloseActionPerformed(evt);
            }
        });
        mFile.add(miClose);

        mbMainFrame.add(mFile);

        mHelp.setText("Help");

        miAbout.setText("About");
        miAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miAboutActionPerformed(evt);
            }
        });
        mHelp.add(miAbout);

        mbMainFrame.add(mHelp);

        mWordWrap.setText("WordWrap");

        miOn.setText("On");
        miOn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miOnActionPerformed(evt);
            }
        });
        mWordWrap.add(miOn);

        miOff.setText("Off");
        miOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miOffActionPerformed(evt);
            }
        });
        mWordWrap.add(miOff);

        mbMainFrame.add(mWordWrap);

        setJMenuBar(mbMainFrame);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lStatusBar, javax.swing.GroupLayout.DEFAULT_SIZE, 873, Short.MAX_VALUE)
            .addComponent(tpOpenedFiles)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tpOpenedFiles, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lStatusBar, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miOpenActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int ret = fc.showOpenDialog(MainFrame.this);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            lStatusBar.setText(file.getPath());
            try {
                try (FileReader fr = new FileReader(file.getPath());
                        BufferedReader br = new BufferedReader(fr)) {
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append(System.lineSeparator());
                    }
                    addPanel(file, sb.toString());
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }//GEN-LAST:event_miOpenActionPerformed

    private void miSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveAsActionPerformed
        showSaveDialog();
    }//GEN-LAST:event_miSaveAsActionPerformed

    private void miCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCloseActionPerformed
        TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
        final JOptionPane op = new JOptionPane();
        for (TextAreaTabPanel pane : map.values()) {
            if (pane.isChanged()) {
                int ret = op.showConfirmDialog(MainFrame.this, "Do you want to save this?", "", JOptionPane.YES_NO_OPTION);
                if (ret == JOptionPane.YES_OPTION) {
                    justSave(panel);
                }
            }
        }
        System.exit(0);

    }//GEN-LAST:event_miCloseActionPerformed

    private void miAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miAboutActionPerformed
        final JOptionPane op = new JOptionPane();
        Icon icon = new ImageIcon("C:\\Users\\Java\\Documents\\NetBeansProjects\\TextEditor\\src\\main\\java\\icons\\java.png");
        op.showMessageDialog(MainFrame.this, "Made by Java students", "About", JOptionPane.OK_OPTION, icon);

    }//GEN-LAST:event_miAboutActionPerformed

    // TODO : napravi new opciju
    private void miNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miNewActionPerformed
        TextAreaTabPanel panel = new TextAreaTabPanel(tpOpenedFiles, null);
        tpOpenedFiles.addTab("test", panel);
        tpOpenedFiles.setTabComponentAt(tpOpenedFiles.indexOfComponent(panel), panel.getTextAreaTabPanelTitle());
        tpOpenedFiles.setSelectedIndex(tpOpenedFiles.getTabCount() - 1);
    }//GEN-LAST:event_miNewActionPerformed

    private void miSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveActionPerformed
        TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
        justSave(panel);
    }//GEN-LAST:event_miSaveActionPerformed

    private void miOnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miOnActionPerformed
        TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
        panel.setWordWrap(true);
    }//GEN-LAST:event_miOnActionPerformed

    private void miOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miOffActionPerformed
        TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
        panel.setWordWrap(false);
    }//GEN-LAST:event_miOffActionPerformed

    /////////////////////        MY METHODS           //////////////////////////
    public void justSave(TextAreaTabPanel panel) throws HeadlessException {
        if (panel.isChanged()) {
            try (PrintWriter pw = new PrintWriter(panel.getFilePath())) {
                pw.write(panel.getText());
                panel.savedFile();

            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            showSaveDialog();
        }
    }

    public void showSaveDialog() throws HeadlessException {
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int ret = fc.showSaveDialog(MainFrame.this);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            lStatusBar.setText("File saved...");
            TextAreaTabPanel panel = (TextAreaTabPanel) tpOpenedFiles.getSelectedComponent();
            panel.setChanged(false);
            try (PrintWriter pw = new PrintWriter(file)) {
                pw.write(panel.getText());
                panel.savedFile();
                lStatusBar.setText(file.getName());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public void addPanel(File file, String text) {
        if (!map.containsKey(file)) {
            TextAreaTabPanel panel = new TextAreaTabPanel(tpOpenedFiles, file);
            panel.setText(text);
            tpOpenedFiles.addTab(file.getName(), panel);
            tpOpenedFiles.setTabComponentAt(tpOpenedFiles.indexOfComponent(panel), panel.getTextAreaTabPanelTitle());
            tpOpenedFiles.setSelectedIndex(tpOpenedFiles.getTabCount() - 1);
            map.put(file, panel);
        } else {
            tpOpenedFiles.setSelectedComponent(map.get(file));
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lStatusBar;
    private javax.swing.JMenu mFile;
    private javax.swing.JMenu mHelp;
    private javax.swing.JMenu mWordWrap;
    private javax.swing.JMenuBar mbMainFrame;
    private javax.swing.JMenuItem miAbout;
    private javax.swing.JMenuItem miClose;
    private javax.swing.JMenuItem miNew;
    private javax.swing.JMenuItem miOff;
    private javax.swing.JMenuItem miOn;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JMenuItem miSaveAs;
    private javax.swing.JTabbedPane tpOpenedFiles;
    // End of variables declaration//GEN-END:variables

}
